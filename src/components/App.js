import React, { useState, useEffect } from 'react';
import { Divider, Grid, Segment } from 'semantic-ui-react';

import AddNotes from './AddNotes';
import NotesList from './NotesList';
import NotesDesc from './NotesDesc';
import SearchBar from './SearchBar';

const App = () =>{
  const [ notesTitles, setNotesTitles ] = useState([]);
  const [ notesTimes, setNotesTimes ] = useState([]);

  const [ title, setTitle ] =useState('');
  const [time, setTime] =useState('');

  const [selectedTitle, setSelectedTitle]= useState('');

  const [searchedTitle, setSearchedTitle] = useState('');

  useEffect(()=>{
    let titles = JSON.parse(localStorage.getItem('titles'));
    setNotesTitles(titles);
    let times = JSON.parse(localStorage.getItem('times'));
    setNotesTimes(times);
  }, [title, time]);

  const onAddClick =(title, time) =>{
    setTitle(title);
    setTime(new Date());

    setNotesTitles([title].concat(notesTitles));
    setNotesTimes([time].concat(notesTimes));
    console.log(JSON.stringify(notesTitles));
  }

  const onNotesSelect =(title) =>{
    setSelectedTitle(title);
  }

  const onTermSubmit = (term) =>{
    setSearchedTitle(term);
  }

  return <div className="ui container" style={{marginTop:'10px'}}>
          <Segment>
            <Grid columns ={2} relaxed ='very' stackable>
              <Grid.Column>
                <div className="ui grid">
                    <div className="row">
                      <div className="ten wide column">
                        <SearchBar data={notesTitles} onSubmit={onTermSubmit}/>
                      </div>
                      <div className="column">
                        <AddNotes onAddClick={onAddClick}/>
                      </div>
                    </div>
                    <div className="row">
                    <div className="column">
                        <NotesList notesTitles={notesTitles}
                        notesTimes={notesTimes}
                        onNotesSelect={()=>onNotesSelect}
                        onSearch={searchedTitle}/>
                      </div>
                    </div>
                  </div>
              </Grid.Column>
              <Grid.Column>
                <NotesDesc title={selectedTitle} />
              </Grid.Column>
            </Grid>

            <Divider vertical>|</Divider>
          </Segment>
        </div>
}

export default App;
