import React from 'react';
import moment from 'moment';


const NotesItem = (props) => {
  return <div className="item" onClick={()=>props.onNotesSelect(props.title)}>
          <div className="content">
            <a className="header">{props.title}</a>
            <p>{moment(props.createdAt).fromNow()}</p>
          </div>
          </div>;
}

export default NotesItem;
