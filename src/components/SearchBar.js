import React, { useState } from 'react';
import ReactSearchBox from 'react-search-box';

const SearchBar = (props) => {
  const [term, setTerm] =useState('');

  const renderedList=[];
  props.data.map((item, index)=> {
    renderedList[index]={'key' : item,
                        'value': item}
  })

  return <div className="search-bar">
          <ReactSearchBox
            value={term}
            data={renderedList}
            callback = {record => console.log(record)}
            onSelect={props.onSubmit(term)}/>
        </div>
}


export default SearchBar;
