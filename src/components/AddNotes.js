import React, { useState } from 'react';
import { Button, Modal } from 'semantic-ui-react';

const AddNotes = (props) => {
    const [ open, setOpen ] = useState(false);
    const [ titles ,setTitles ] = useState([]);
    const [title, setTitle ] = useState('');
    const [times, setTimes] =useState([]);
    const [time, setTime] =useState('');

    const onFormSubmit = (e) =>{
      e.preventDefault();
      setTime(new Date());

      setTitles([title].concat(titles));
      setTimes([time].concat(times));

      setOpen(false);
      setTitle('');
      props.onAddClick(title, time);
      
      localStorage.setItem('times',JSON.stringify(times));
      localStorage.setItem('titles', JSON.stringify(titles));
  }

    return (
        <Modal
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
        trigger={<Button><i className="add icon"></i></Button>}
        >
        <Modal.Header>Add Notes</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <form onSubmit={onFormSubmit} className="ui form" style={{padding:'10px'}}>
            <div className="content">
              <div className="field">
                <label>Title</label>
                <input type="text" value={title} onChange={(e)=>setTitle(e.target.value)}/>
              </div>
                <input type="hidden" value={time} onChange={(e)=>setTime(e.target.value)}/>
              </div>
            </form>
          </Modal.Description>
        </Modal.Content>
        </Modal>
  )
}

export default AddNotes;
