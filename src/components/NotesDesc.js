import React, { useState } from 'react';


const NotesDesc = (props) =>{
  const [ desc, setDesc ] = useState('');

  return <div className="ui form">
          <div className="field">
            <label><h2>{props.title}</h2></label>
            <textarea rows="20" cols="60"
            value={desc}
            onChange={(e)=>setDesc(e.target.value)}></textarea>
          </div>
        </div>;
}

export default NotesDesc;
