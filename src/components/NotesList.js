import React from 'react';

import NotesItem from './NotesItem';

const NotesList = (props) =>{
  if(!props.notesTitles)
    return <div>Loading..</div>
  const renderedList = props.notesTitles.map((noteTitle, index) =>{
      return <NotesItem key={noteTitle}
      title={noteTitle}
      createdAt={props.notesTimes[index]}
      onNotesSelect={props.onNotesSelect(noteTitle)}/>
  });

  return <div className="ui relaxed divided list">
          {renderedList}
        </div>;
}


export default NotesList;
